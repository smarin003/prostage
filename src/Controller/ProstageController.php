<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Entreprise;
use App\Entity\Formation;
use App\Entity\Stage;
use App\Form\EntrepriseType;
use App\Form\StageType;
use Symfony\Component\HttpFoundation\Request;

class ProstageController extends AbstractController
{
  /**
   * @Route("/", name="accueil")
   */
  public function index()
  {
    $lesStages = $this->getDoctrine()->getRepository(Stage::class)->findAll();
    return $this->render("prostage/index.html.twig", [
      'message' => 'Bienvenue sur l\'accueil de mon site',
      'lesStages' => $lesStages,
    ]);
  }

  /**
   * @Route("/modif/Entreprise/{id}", name="modifEntreprise")
   */
  public function modifiEntreprise(Request $request, ObjectManager $manager, Entreprise $entreprise)
  {

    $formulaireEntreprise = $this->createForm(EntrepriseType::class, $entreprise)
      ->handleRequest($request);

    if ($formulaireEntreprise->isSubmitted() && $formulaireEntreprise->isValid()) {

      $manager->persist($entreprise);
      $manager->flush();

      return $this->redirectToRoute('entreprises');
    }

    return $this->render("prostage/ajoutModifEntreprise.html.twig", [
      'vueFormulaire' => $formulaireEntreprise->createView(), 'action' => "modifier"
    ]);
  }


  /**
   * @Route("/ajout/Entreprise", name="ajoutEntreprise")
   */
  public function ajouterEntreprise(Request $request, ObjectManager $manager)
  {
    $entreprise = new Entreprise();

    $formulaireEntreprise = $this->createForm(EntrepriseType::class, $entreprise)
      ->handleRequest($request);

    if ($formulaireEntreprise->isSubmitted() && $formulaireEntreprise->isValid()) {

      $manager->persist($entreprise);
      $manager->flush();

      return $this->redirectToRoute('entreprises');
    }

    return $this->render("prostage/ajoutModifEntreprise.html.twig", [
      'vueFormulaire' => $formulaireEntreprise->createView(), 'action' => "ajouter"
    ]);
  }

  /**
   * @Route("/modif/Stage/{id}", name="modifStage")
   */
  public function modifStage(Request $request, ObjectManager $manager, Stage $stage)
  {

    $formulaireStage = $this->createForm(StageType::class, $stage)
      ->handleRequest($request);

    if ($formulaireStage->isSubmitted() && $formulaireStage->isValid()) {

      $manager->persist($stage);
      $manager->flush();

      return $this->redirectToRoute('accueil');
    }

    return $this->render("prostage/ajoutModifStage.html.twig", [
      'vueFormulaire' => $formulaireStage->createView(), 'action' => "modifier"
    ]);
  }

  /**
   * @Route("/ajout/Stage", name="ajoutStage")
   */
  public function ajoutStage(Request $request, ObjectManager $manager)
  {
    $stage = new Stage();

    $formulaireStage = $this->createForm(StageType::class, $stage)
      ->handleRequest($request);

    if ($formulaireStage->isSubmitted() && $formulaireStage->isValid()) {

      $manager->persist($stage);
      $manager->flush();

      return $this->redirectToRoute('accueil');
    }

    return $this->render("prostage/ajoutModifStage.html.twig", [
      'vueFormulaire' => $formulaireStage->createView(), 'action' => "ajouter"
    ]);
  }

  /**
   * @Route("/byEntreprise/{nomEntreprise}", name="nomEntreprise")
   */
  public function indexByEntreprise(string $nomEntreprise)
  {
    $lesStages = $this->getDoctrine()->getRepository(Stage::class)->findByEntreprise($nomEntreprise);
    return $this->render("prostage/index.html.twig", [
      'message' => "Voici les stages proposé par $nomEntreprise",
      'nomEntreprise' => $nomEntreprise,
      'lesStages' => $lesStages,
    ]);
  }

  /**
   * @Route("/byFormation/{nomFormation}", name="nomFormation")
   */
  public function indexByFormation(string $nomFormation)
  {
    $lesStages = $this->getDoctrine()->getRepository(Stage::class)->findByFormation($nomFormation);
    return $this->render("prostage/index.html.twig", [
      'message' => "Voici les stages proposé par $nomFormation",
      'nomFormation' => $nomFormation,
      'lesStages' => $lesStages,
    ]);
  }

  /**
   * @Route("/stage/{id}", name="stage")
   */
  public function stage(int $id) //affiche la page du stage
  {
    $stage = $this->getDoctrine()->getRepository(Stage::class)->find($id);
    return $this->render("prostage/stage.html.twig", [
      'message' => "Vous êtes sur le stage $id",
      'id' => $id,
      "stage" => $stage,
    ]);
  }

  /**
   * @Route("/entreprises", name="entreprises")
   */
  public function listeEntreprises() //affiche la liste des entreprises
  {
    $listeEntreprises = $this->getDoctrine()->getRepository(Entreprise::class)->findAll();
    return $this->render("prostage/entreprise.html.twig", [
      'message' => 'Liste des entreprises',
      "listeEntreprises" => $listeEntreprises,
    ]);
  }
}
