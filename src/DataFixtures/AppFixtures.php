<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Formation;
use App\Entity\User;
use Faker;

class AppFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    // On configure dans quelles langues nous voulons nos données
    $faker = Faker\Factory::create('fr_FR');

    //creation de user
    //On ne devrait pas voir les mots de passe en clair mais je les ai mis de la sorte au cas où vous voudriez effectuer des tests
    $smarin003 = new User();
    $smarin003->setUsername('smarin003');
    $smarin003->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
    //j'ai utilisé un script php de la forme ci dessous 
    $password = 'Pasiblo1109'; 
    $hash = password_hash($password, PASSWORD_ARGON2I);
    $smarin003->setPassword($hash);
    $manager->persist($smarin003);

    $smarin003 = new User();
    $smarin003->setUsername('sidm1');
    $smarin003->setRoles(['ROLE_USER']);
    $password = 'test';
    $hash = password_hash($password, PASSWORD_ARGON2I);
    $smarin003->setPassword($hash);
    $manager->persist($smarin003);

    $smarin003 = new User();
    $smarin003->setUsername('sidm2');
    $smarin003->setRoles(['ROLE_ADMIN']);
    $password = 'motdepasse';
    $hash = password_hash($password, PASSWORD_ARGON2I);
    $smarin003->setPassword($hash);
    $manager->persist($smarin003);

    //on créer un exemple sans faker
    $stage = new Stage();
    $stage->setTitre("exemple de stage");
    $stage->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
    $stage->setDateDebut($faker->DateTime($max = 'now'));
    $stage->setDateFin($faker->DateTime($max = 'now'));
    $stage->setActivite($faker->companyEmail);

    $entreprise = new Entreprise();
    $entreprise->setNom("Safran");
    $entreprise->setActivite("unActivite");
    $entreprise->setAdresse($faker->address);
    $entreprise->setNumTel($faker->phoneNumber);
    $entreprise->setSite($faker->url);
    $entreprise->addStage($stage);

    $formation = new Formation();
    $formation->setType($faker->randomElement($array = array('DUT Informatique', 'LP Prog', 'LP Num', 'DUT TIC', 'DUT GEA', 'fac Biologie')));
    $formation->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
    $formation->addStage($stage);

    $stage->setEntreprise($entreprise);
    $stage->addFormation($formation);

    $manager->persist($stage);
    $manager->persist($formation);
    $manager->persist($entreprise);


    // on créé 10 personnes
    for ($i = 0; $i < 10; $i++) {
      $stage = new Stage();
      $stage->setTitre($faker->jobTitle);
      $stage->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
      $stage->setDateDebut($faker->DateTime($max = 'now'));
      $stage->setDateFin($faker->DateTime($max = 'now'));
      $stage->setActivite($faker->companyEmail);

      $entreprise = new Entreprise();
      $entreprise->setNom($faker->company);
      $entreprise->setActivite("unActivite");
      $entreprise->setAdresse($faker->address);
      $entreprise->setNumTel($faker->phoneNumber);
      $entreprise->setSite($faker->url);
      $entreprise->addStage($stage);

      $formation = new Formation();
      $formation->setType($faker->randomElement($array = array('DUT Informatique', 'LP Prog', 'LP Num', 'DUT TIC', 'DUT GEA', 'fac Biologie')));
      $formation->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
      $formation->addStage($stage);

      $stage->setEntreprise($entreprise);
      $stage->addFormation($formation);

      $manager->persist($stage);
      $manager->persist($formation);
      $manager->persist($entreprise);
    }
    $manager->flush();
  }
}
