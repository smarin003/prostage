<?php

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntrepriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, ['attr'=>['placeholder'=>"Nom de l'entreprise"]])
            ->add('activite', TextareaType::class, ['attr'=>['placeholder'=>"ex : une description des activités de l'entreprise"]])
            ->add('adresse', TextType::class, ['attr'=>['placeholder'=>"ex : 6 bis chemin de l'exemple BOUCAU 64340"]])
            ->add('numTel', TelType::class, ['attr'=>['placeholder'=>"ex : 0011223344"]])
            ->add('site', UrlType::class, ['attr'=>['placeholder'=>"ex : http://www.exemple.com"]])
            ->add('Valider', SubmitType::class, ['label'=>'enregistrer'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entreprise::class,
        ]);
    }
}